#include "vm/page.h"
#include "threads/pte.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "threads/malloc.h"
#include "filesys/file.h"
#include "string.h"
#include "userprog/syscall.h"
#include "userprog/process.h"

// Added by Adrian Colesa - VM
// The following functions were taken from the Pintos manual (section A.8.5 Hash Table Example)

/* Returns a hash value for page p. */
unsigned
page_hash (const struct hash_elem *p_, void *aux UNUSED)
{
	const struct supl_pte *p = hash_entry (p_, struct supl_pte, he);
	return hash_int (p->virt_page_no);
}

/* Returns true if page a precedes page b. */
bool
page_less (const struct hash_elem *a_, const struct hash_elem *b_,
void *aux UNUSED)
{
	const struct supl_pte *a = hash_entry (a_, struct supl_pte, he);
	const struct supl_pte *b = hash_entry (b_, struct supl_pte, he);

	return a->virt_page_no < b->virt_page_no;
}


/*
 * Returns the supplemental page table entry, containing the given virtual address,
 * or a null pointer if no such page exists.
*/
struct supl_pte *
page_lookup (const void *pg_no)
{
	struct supl_pte spte;
	struct hash_elem *e;

	spte.virt_page_no = pg_no;
	e = hash_find (&thread_current()->supl_pt, &spte.he);

	return e != NULL ? hash_entry (e, struct supl_pte, he) : NULL;
}