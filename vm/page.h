#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <stdio.h>
#include "threads/thread.h"
#include "threads/palloc.h"
#include "lib/kernel/hash.h"
#include "filesys/file.h"

// Added by Adrian Colesa - VM
#ifdef VM
struct supl_pte {		// an entry (element) in the supplemental page table
	uint32_t	virt_page_no; 			// the number of the virtual page (also the hash key) having info about
	void*		virt_page_addr;			// the virtual address of the page
	off_t 		ofs;					// the offset in file the bytes to be stored in the page must be read from
	size_t 		page_read_bytes;		// number of bytes to read from the file and store in the page
	size_t 		page_zero_bytes; 		// number of bytes to zero in the page
	bool		writable;				// indincate if the page is writable or not
	struct hash_elem he;				// element to insert the structure in a hash list
};
#endif

/* Returns a hash value for page p. */
unsigned page_hash (const struct hash_elem *p_, void *aux UNUSED);
/* Returns true if page a precedes page b. */
bool page_less (const struct hash_elem *a_, const struct hash_elem *b_, void *aux UNUSED);
/*
 * Returns the supplemental page table entry, containing the given virtual address,
 * or a null pointer if no such page exists.
*/
struct supl_pte* page_lookup (const void *address);


#endif /* vm/page.h */