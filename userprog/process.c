#include "userprog/process.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "userprog/gdt.h"
#include "userprog/pagedir.h"
#include "userprog/tss.h"
#include "filesys/directory.h"
#include "filesys/file.h"
#include "filesys/filesys.h"
#include "threads/flags.h"
#include "threads/init.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"

/* Added by Adrian Colesa - multithreading */
#include <syscall-nr.h>

static thread_func start_process NO_RETURN;
static bool load (const char *cmdline, void (**eip) (void), void **esp);
void push_arguments_on_stack(const char **arg_array, int count, void **esp);

/* Starts a new thread running a user program loaded from
   FILENAME.  The new thread may be scheduled (and may even exit)
   before process_execute() returns.  Returns the new process's
   thread id, or TID_ERROR if the thread cannot be created. */
tid_t
process_execute (const char *console_arg) 
{
  char *console_arg_copy;
  tid_t tid;

  /* Make a copy of FILE_NAME.
     Otherwise there's a race between the caller and load(). */
  console_arg_copy = palloc_get_page (0);
  if (console_arg_copy == NULL)
    return TID_ERROR;
  strlcpy (console_arg_copy, console_arg, PGSIZE);

  // Denisa: get the name for the thread
  char *copy = (char *) malloc(strlen(console_arg) + 1);
  char *copy_secure;
  char *file_name = (char *) malloc(strlen(console_arg) + 1);

  memcpy(copy, console_arg, strlen(console_arg) + 1);
  copy = strtok_r(copy, " ", &copy_secure);
  strlcpy(file_name, copy, strlen(console_arg) + 1);
  free(copy);
  // Denisa end

  /* Create a new thread to execute FILE_NAME. */
  tid = thread_create (file_name, PRI_DEFAULT, start_process, console_arg_copy);
  /* Added by Mihai Deac */
  if (tid != TID_ERROR)
    {
      struct list_elem *e;

      for (e = list_begin (&thread_current()->p->children); e != list_end (&thread_current()->p->children);
          e = list_next (e))
        {
          struct process *p = list_entry (e, struct process, elem);
          if (p->pid == tid)
          {
            sema_down(&p->sema);

            if (p->load_flag != LOADED)
            {
              tid = TID_ERROR;
            }
          }
        }
    }
  /* end Mihai Deac */

  // if (tid == TID_ERROR)
    // palloc_free_page (console_arg_copy); 
  return tid;
}

/* A thread function that loads a user process and starts it
   running. */
static void
start_process (void *console_arg_)
{
  struct process* p = thread_current()->p;
  p->cmd_line = console_arg_;
  // end Mihai

  char *console_arg = console_arg_;
  struct intr_frame if_;
  bool success;

  // Added by Adrian Colesa - VM
  #ifdef VM
    hash_init(&thread_current()->supl_pt, page_hash, page_less, NULL);
    IF_PRINT1 printf("Suplimental page (hash) table initialized\n");
    thread_current()->exec_file = NULL;
  #endif

  // Denisa: get the name for the thread and parse the args
  char *console_arg_copy = (char *) malloc(strlen(console_arg) + 1);
  char *copy_secure;
  char *token;
  int limit_nr_arg = 30;
  int limit_size_arg = 100;
  int count = 0;
  char **arg_array = (char**) malloc(limit_nr_arg * sizeof(char*));
  for(int i = 0; i < limit_nr_arg; i++){
    arg_array[i] = (char*) malloc(limit_size_arg * sizeof(char));
  }

  memcpy(console_arg_copy, console_arg, strlen(console_arg) + 1);

  token = strtok_r(console_arg_copy, " ", &copy_secure);
  while(token != NULL){
    strlcpy(arg_array[count++], token, limit_size_arg);
		token = strtok_r(NULL, " ", &copy_secure);
  }
  //print
  IF_PRINT1 
  for(int i = 0; i < count; i++){
    printf("arg_array[%d]: %s\n", i, arg_array[i]);
  }
  // Denisa end

  /* Initialize interrupt frame and load executable. */
  memset (&if_, 0, sizeof if_);
  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
  if_.cs = SEL_UCSEG;
  if_.eflags = FLAG_IF | FLAG_MBS;
  // Denisa: 
  success = load (arg_array[0], &if_.eip, &if_.esp);
  // Denisa end
  
  /* If load failed, quit. */
  palloc_free_page (console_arg);
  /* Added by Mihai Deac */
  IF_PRINT1 printf("succcess_flag = %d\n", success);
  if (!success)
    { 
	    // p->pid = TID_ERROR;
      p->pid = thread_current()->tid;
      thread_exit (TID_ERROR);
      return;
    } 
  else
    {
      p->load_flag = LOADED;
      push_arguments_on_stack(arg_array, count, &if_.esp);
      // TODO: push args
    }
    
  // sema_up(&p->sema);
  /* end Mihai Deac */

  /* Added by Adrian Colesa - multithreading */
  thread_current()->pid = thread_current()->tid;	// the main thread has pid = tid
  sema_up(&p->sema);

  /* Start the user process by simulating a return from an
     interrupt, implemented by intr_exit (in
     threads/intr-stubs.S).  Because intr_exit takes all of its
     arguments on the stack in the form of a `struct intr_frame',
     we just point the stack pointer (%esp) to our stack frame
     and jump to it. */
  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
  NOT_REACHED ();
}

// Denisa: 
void push_arguments_on_stack(const char **arg_array, int argc, void **esp){

  int *arg_addr = (int*) malloc(argc * sizeof(int));
  int length;

  //pune pe stiva valorile
  for(int i = 0; i < argc; i++){
    length = strlen(arg_array[i]) + 1;
    *esp -= length;
    memcpy(*esp, arg_array[i], length);
    arg_addr[i] = (int)*esp;
  }

  //alinieaza stiva
  *esp = (void*)((unsigned int)(*esp) & 0xfffffffc);

  //ultimul NULL
  *esp -= 4;
  
  *(int *)*esp = 0;

  //pune pe stiva adresele  
  for(int i = argc - 1; i >= 0; i--){
    *esp -= 4;
    *((void**) *esp) = arg_addr[i];
  }

  //pune pe stiva argv, argc, 0
  *esp -= 4;
  *(void **)*esp = (*esp + 4);
  *esp -= 4;
  *(int *)*esp = argc;
  *esp -= 4;
  *(int *)*esp = 0;
  // Denisa end
}

/* Waits for thread TID to die and returns its exit status.  If
   it was terminated by the kernel (i.e. killed due to an
   exception), returns -1.  If TID is invalid or if it was not a
   child of the calling process, or if process_wait() has already
   been successfully called for the given TID, returns -1
   immediately, without waiting.

   This function will be implemented in problem 2-2.  For now, it
   does nothing. */
int
process_wait (tid_t child_tid UNUSED) 
{
	// Added by Adrian Colesa - multithreading
  IF_PRINT1 	printf("Thread %s that started the testing program (and corresponding thread) waits after created process to finish its execution\n", thread_current()->name);
	// while (1);

  /* Added by Mihai Deac */
  struct process* child = NULL;
  struct list_elem *e;
  int to_return = TID_ERROR;

  for (e = list_begin (&thread_current()->p->children); e != list_end (&thread_current()->p->children);
      e = list_next (e))
    {
      struct process *p = list_entry (e, struct process, elem);
      if (p->pid == child_tid)
      {
        child = p;
        break;
      }
    }

  if (child == NULL)
  {
    // IF_PRINT0 printf("(%s) %d\n", (char*)thread_current()->name, TID_ERROR);
    return TID_ERROR;
  }
  if (child->waiting_flag == WAITING)
  {
    // IF_PRINT0 printf("(%s) %d\n", (char*)thread_current()->name, TID_ERROR);
    return TID_ERROR;
  }
  child->waiting_flag = WAITING;
  sema_down(&child->sema);
  to_return = child->exit_code;
  // IF_PRINT0 printf("(%s) %d\n", (char*)thread_current()->name, TID_ERROR);
  list_remove(&child->elem);
  palloc_free_page(child);
  return to_return;
  /* end Mihai Deac */

	// return -1;
}

/* Added by Mihai deac */
int get_process_type(struct process *p)
{
  if (!list_empty(&p->children))
  {
    IF_PRINT1 printf("PARENT\n");
    return PARENT;
  }
  if (list_empty(&p->children) && p->without_parent_flag == true)
  {
    IF_PRINT1 printf("PARENTLESS\n");
    return PARENTLESS;
  }
  if (list_empty(&p->children) && p->without_parent_flag == false)
  {
    IF_PRINT1 printf("CHILD\n");
    return CHILD;
  }
  return TID_ERROR;
}
/* end Mihai Deac */

/* Free the current process's resources. */
void
process_exit (int code)
{
  struct thread *cur = thread_current ();
  struct process *p;  
  int process_type; 
  uint32_t *pd;

  // Added by Adrian Colesa - VM
  //Denisa: allow write
  if (cur->exec_file != NULL)
      if(cur->exec_file) {
        file_allow_write(cur->exec_file);
        file_close(cur->exec_file);
  }

  /* Destroy the current process's page directory and switch back
     to the kernel-only page directory. */
  pd = cur->pagedir;
  if (pd != NULL) 
    {

      /* Added by Mihai Deac */
      p = thread_current()->p;
      if (p != NULL)
      {
        process_type = get_process_type(p);

        if (process_type == PARENT)
        {
          struct list_elem *e;
          struct list_elem *tail = list_end (&p->children);
          while (e != tail && !list_empty(&p->children))
          {
            for (e = list_begin (&p->children); e != list_end (&p->children);
            e = list_next (e))
            {
              struct process *child = list_entry (e, struct process, elem);
              if (child->exit_flag == EXITED) 
              {
                list_remove(&child->elem);
                palloc_free_page(child); //TODO
                break;
              }
            }
          }
          // for (e = list_begin (&p->children); e != list_end (&p->children);
          //     e = list_next (e))
          //   {
          //     struct process *child = list_entry (e, struct process, elem);
          //     if (child->exit_flag == EXITED)
          //     {
          //       list_remove(&child->elem);
          //       e = list_next(e);
          //       palloc_free_page(child); //TODO
          //     }
          //   }
            process_type = get_process_type(p);
        }
        if (process_type == CHILD)
        {
          p->exit_code = code;
          p->exit_flag = EXITED;
        }
        if (process_type == PARENTLESS)
        {
          palloc_free_page(p);
        }

      // p->exit_code = code;

      IF_PRINT0 printf("%s: exit(%d)\n", (char*)thread_current()->name, code);
      
      // sema_up(&p->sema);

      // printf("Exited\n");
      /* end MIhai Deac */

        /* Added by Adrian Colesa - multithreading */
        /* Only the if and then branch was added, the else was here initialy */
        if (cur->pid != cur->tid)	{ // not the main thread of the process
          cur->pagedir = NULL;
          pagedir_activate (NULL);
        } else {
          /* Correct ordering here is crucial.  We must set
          cur->pagedir to NULL before switching page directories,
          so that a timer interrupt can't switch back to the
          process page directory.  We must activate the base page
          directory before destroying the process's page
          directory, or our active page directory will be one
          that's been freed (and cleared). */
          cur->pagedir = NULL;
          pagedir_activate (NULL);
          pagedir_destroy (pd);
        }
      }
      sema_up(&p->sema);
    }
}

/* Sets up the CPU for running user code in the current
   thread.
   This function is called on every context switch. */
void
process_activate (void)
{
  struct thread *t = thread_current ();

  /* Activate thread's page tables. */
  pagedir_activate (t->pagedir);

  /* Set thread's kernel stack for use in processing
     interrupts. */
  tss_update ();
}

/* We load ELF binaries.  The following definitions are taken
   from the ELF specification, [ELF1], more-or-less verbatim.  */

/* ELF types.  See [ELF1] 1-2. */
typedef uint32_t Elf32_Word, Elf32_Addr, Elf32_Off;
typedef uint16_t Elf32_Half;

/* For use with ELF types in printf(). */
#define PE32Wx PRIx32   /* Print Elf32_Word in hexadecimal. */
#define PE32Ax PRIx32   /* Print Elf32_Addr in hexadecimal. */
#define PE32Ox PRIx32   /* Print Elf32_Off in hexadecimal. */
#define PE32Hx PRIx16   /* Print Elf32_Half in hexadecimal. */

/* Executable header.  See [ELF1] 1-4 to 1-8.
   This appears at the very beginning of an ELF binary. */
struct Elf32_Ehdr
  {
    unsigned char e_ident[16];
    Elf32_Half    e_type;
    Elf32_Half    e_machine;
    Elf32_Word    e_version;
    Elf32_Addr    e_entry;
    Elf32_Off     e_phoff;
    Elf32_Off     e_shoff;
    Elf32_Word    e_flags;
    Elf32_Half    e_ehsize;
    Elf32_Half    e_phentsize;
    Elf32_Half    e_phnum;
    Elf32_Half    e_shentsize;
    Elf32_Half    e_shnum;
    Elf32_Half    e_shstrndx;
  };

/* Program header.  See [ELF1] 2-2 to 2-4.
   There are e_phnum of these, starting at file offset e_phoff
   (see [ELF1] 1-6). */
struct Elf32_Phdr
  {
    Elf32_Word p_type;
    Elf32_Off  p_offset;
    Elf32_Addr p_vaddr;
    Elf32_Addr p_paddr;
    Elf32_Word p_filesz;
    Elf32_Word p_memsz;
    Elf32_Word p_flags;
    Elf32_Word p_align;
  };

/* Values for p_type.  See [ELF1] 2-3. */
#define PT_NULL    0            /* Ignore. */
#define PT_LOAD    1            /* Loadable segment. */
#define PT_DYNAMIC 2            /* Dynamic linking info. */
#define PT_INTERP  3            /* Name of dynamic loader. */
#define PT_NOTE    4            /* Auxiliary info. */
#define PT_SHLIB   5            /* Reserved. */
#define PT_PHDR    6            /* Program header table. */
#define PT_STACK   0x6474e551   /* Stack segment. */

/* Flags for p_flags.  See [ELF3] 2-3 and 2-4. */
#define PF_X 1          /* Executable. */
#define PF_W 2          /* Writable. */
#define PF_R 4          /* Readable. */

static bool setup_stack (void **esp);
static bool validate_segment (const struct Elf32_Phdr *, struct file *);
static bool load_segment (struct file *file, off_t ofs, uint8_t *upage,
                          uint32_t read_bytes, uint32_t zero_bytes,
                          bool writable);
static bool load_segment_lazily (struct file *file, off_t ofs, uint8_t *upage,
		                      uint32_t read_bytes, uint32_t zero_bytes, 
                          bool writable);                           

/* Loads an ELF executable from FILE_NAME into the current thread.
   Stores the executable's entry point into *EIP
   and its initial stack pointer into *ESP.
   Returns true if successful, false otherwise. */
bool
load (const char *file_name, void (**eip) (void), void **esp) 
{
  struct thread *t = thread_current ();
  struct Elf32_Ehdr ehdr;
  struct file *file = NULL;
  off_t file_ofs;
  bool success = false;
  int i;

  /* Allocate and activate page directory. */
  t->pagedir = pagedir_create ();
  if (t->pagedir == NULL) 
    goto done;
  process_activate ();

  /* Open executable file. */
  file = filesys_open (file_name);
  if (file == NULL) 
    {
      printf ("load: %s: open failed\n", file_name);
      goto done; 
    }

  /* Read and verify executable header. */
  if (file_read (file, &ehdr, sizeof ehdr) != sizeof ehdr
      || memcmp (ehdr.e_ident, "\177ELF\1\1\1", 7)
      || ehdr.e_type != 2
      || ehdr.e_machine != 3
      || ehdr.e_version != 1
      || ehdr.e_phentsize != sizeof (struct Elf32_Phdr)
      || ehdr.e_phnum > 1024) 
    {
      printf ("load: %s: error loading executable\n", file_name);
      goto done; 
    }

  /* Read program headers. */
  file_ofs = ehdr.e_phoff;
  for (i = 0; i < ehdr.e_phnum; i++) 
    {
      struct Elf32_Phdr phdr;

      if (file_ofs < 0 || file_ofs > file_length (file))
        goto done;
      file_seek (file, file_ofs);

      if (file_read (file, &phdr, sizeof phdr) != sizeof phdr)
        goto done;
      file_ofs += sizeof phdr;
      switch (phdr.p_type) 
        {
        case PT_NULL:
        case PT_NOTE:
        case PT_PHDR:
        case PT_STACK:
        default:
          /* Ignore this segment. */
          break;
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_SHLIB:
          goto done;
        case PT_LOAD:
          if (validate_segment (&phdr, file)) 
            {
              bool writable = (phdr.p_flags & PF_W) != 0;
              uint32_t file_page = phdr.p_offset & ~PGMASK;
              uint32_t mem_page = phdr.p_vaddr & ~PGMASK;
              uint32_t page_offset = phdr.p_vaddr & PGMASK;
              uint32_t read_bytes, zero_bytes;
              if (phdr.p_filesz > 0)
                {
                  /* Normal segment.
                     Read initial part from disk and zero the rest. */
                  read_bytes = page_offset + phdr.p_filesz;
                  zero_bytes = (ROUND_UP (page_offset + phdr.p_memsz, PGSIZE)
                                - read_bytes);
                }
              else 
                {
                  /* Entirely zero.
                     Don't read anything from disk. */
                  read_bytes = 0;
                  zero_bytes = ROUND_UP (page_offset + phdr.p_memsz, PGSIZE);
                }

              // Added by Adrian Colesa
              /*
              if (!writable)
            	  printf("[load] Loading CODE segment of %d bytes size starting from virtual memory 0x%x (physical memory 0x%x)\n", read_bytes, phdr.p_vaddr, phdr.p_paddr);
              else
            	  printf("[load] Loading DATA segment of %d bytes size starting from virtual memory 0x%x (physical memory 0x%x)\n", read_bytes, phdr.p_vaddr, phdr.p_paddr);
               */

              if (!load_segment_lazily (file, file_page, (void *) mem_page,
                                 read_bytes, zero_bytes, writable))
                goto done;
            }
          else
            goto done;
          break;
        }
    }

  /* Set up stack. */
  if (!setup_stack (esp))
    goto done;

  /* Start address. */
  *eip = (void (*) (void)) ehdr.e_entry;

  success = true;

 done:
  /* We arrive here whether the load is successful or not. */
  // Added by Adrian Colesa - VM
  #ifdef VM
  if (success == true){
    //Denisa: deny_write
    file_deny_write (file);
    thread_current()->exec_file = file;
  }    
  #else
    file_close (file);
  #endif

  return success;
}

/* load() helpers. */

static bool install_page (void *upage, void *kpage, bool writable);

/* Checks whether PHDR describes a valid, loadable segment in
   FILE and returns true if so, false otherwise. */
static bool
validate_segment (const struct Elf32_Phdr *phdr, struct file *file) 
{
  /* p_offset and p_vaddr must have the same page offset. */
  if ((phdr->p_offset & PGMASK) != (phdr->p_vaddr & PGMASK)) 
    return false; 

  /* p_offset must point within FILE. */
  if (phdr->p_offset > (Elf32_Off) file_length (file)) 
    return false;

  /* p_memsz must be at least as big as p_filesz. */
  if (phdr->p_memsz < phdr->p_filesz) 
    return false; 

  /* The segment must not be empty. */
  if (phdr->p_memsz == 0)
    return false;
  
  /* The virtual memory region must both start and end within the
     user address space range. */
  if (!is_user_vaddr ((void *) phdr->p_vaddr))
    return false;
  if (!is_user_vaddr ((void *) (phdr->p_vaddr + phdr->p_memsz)))
    return false;

  /* The region cannot "wrap around" across the kernel virtual
     address space. */
  if (phdr->p_vaddr + phdr->p_memsz < phdr->p_vaddr)
    return false;

  /* Disallow mapping page 0.
     Not only is it a bad idea to map page 0, but if we allowed
     it then user code that passed a null pointer to system calls
     could quite likely panic the kernel by way of null pointer
     assertions in memcpy(), etc. */
  if (phdr->p_vaddr < PGSIZE)
    return false;

  /* It's okay. */
  return true;
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE lazily */
static bool
load_segment_lazily (struct file *file, off_t ofs, uint8_t *upage,
		     uint32_t read_bytes, uint32_t zero_bytes, bool writable) 
{
  // Added by Adrian Colesa - VM
	struct supl_pte *spte;

  ASSERT ((read_bytes + zero_bytes) % PGSIZE == 0);
  ASSERT (pg_ofs (upage) == 0);
  ASSERT (ofs % PGSIZE == 0);

  // Added by Adrian Colesa - VM
  off_t crt_ofs = ofs;

  while (read_bytes > 0 || zero_bytes > 0) 
  {
    /* Calculate how to fill this page.
      We will read PAGE_READ_BYTES bytes from FILE
      and zero the final PAGE_ZERO_BYTES bytes. */
    size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
    size_t page_zero_bytes = PGSIZE - page_read_bytes;

    // Added by Adrian Colesa - VM
    #ifdef VM
    spte = malloc(sizeof(struct supl_pte));
    spte->virt_page_addr = upage;
    spte->virt_page_no = ((unsigned int) upage)/PGSIZE;
    spte->ofs = crt_ofs;
    spte->page_read_bytes = page_read_bytes;
    spte->page_zero_bytes = page_zero_bytes;
    spte->writable = writable;
    hash_insert (&thread_current()->supl_pt, &spte->he);

    IF_PRINT1  printf("[load_segment] spte->virt_page_addr=0x%x, spte->virt_page_no=%d, spte->ofs=%d, spte->page_read_bytes=%d, spte->page_zero_bytes=%d, spte->writable=%d\n",
    spte->virt_page_addr, spte->virt_page_no, spte->ofs, spte->page_read_bytes, spte->page_zero_bytes, spte->writable);

    crt_ofs += page_read_bytes;
    #endif

    /* Advance. */
    read_bytes -= page_read_bytes;
    zero_bytes -= page_zero_bytes;
    upage += PGSIZE;
  }
  return true;
  
}

/* Loads a segment starting at offset OFS in FILE at address
   UPAGE.  In total, READ_BYTES + ZERO_BYTES bytes of virtual
   memory are initialized, as follows:

        - READ_BYTES bytes at UPAGE must be read from FILE
          starting at offset OFS.

        - ZERO_BYTES bytes at UPAGE + READ_BYTES must be zeroed.

   The pages initialized by this function must be writable by the
   user process if WRITABLE is true, read-only otherwise.

   Return true if successful, false if a memory allocation error
   or disk read error occurs. */
static bool
load_segment (struct file *file, off_t ofs, uint8_t *upage,
              uint32_t read_bytes, uint32_t zero_bytes, bool writable) 
{
  ASSERT ((read_bytes + zero_bytes) % PGSIZE == 0);
  ASSERT (pg_ofs (upage) == 0);
  ASSERT (ofs % PGSIZE == 0);

  file_seek (file, ofs);
  while (read_bytes > 0 || zero_bytes > 0) 
    {
      /* Calculate how to fill this page.
         We will read PAGE_READ_BYTES bytes from FILE
         and zero the final PAGE_ZERO_BYTES bytes. */
      size_t page_read_bytes = read_bytes < PGSIZE ? read_bytes : PGSIZE;
      size_t page_zero_bytes = PGSIZE - page_read_bytes;

      /* Get a page of memory. */
      uint8_t *kpage = palloc_get_page (PAL_USER);
      if (kpage == NULL)
        return false;

      /* Load this page. */
      if (file_read (file, kpage, page_read_bytes) != (int) page_read_bytes)
        {
          palloc_free_page (kpage);
          return false; 
        }
      memset (kpage + page_read_bytes, 0, page_zero_bytes);

      // Added by Adrian Colesa
      /*
      printf("[load_segment] The process virtual page %d starting at virtual address 0x%x will be mapped onto the kernel virtual page %d (physical frame %d) starting at kernel virtual address 0x%x (physical address 0x%x)\n", ((unsigned int) upage)/PGSIZE, upage, (unsigned int)kpage/PGSIZE, ((unsigned int)vtop(kpage))/PGSIZE, kpage, vtop(kpage));
       */

      /* Add the page to the process's address space. */
      if (!install_page (upage, kpage, writable)) 
        {
          palloc_free_page (kpage);
          return false; 
        }

      /* Advance. */
      read_bytes -= page_read_bytes;
      zero_bytes -= page_zero_bytes;
      upage += PGSIZE;
    }
  return true;
}

/* Create a minimal stack by mapping a zeroed page at the top of
   user virtual memory. */
static bool
setup_stack (void **esp) 
{
  uint8_t *kpage;
  bool success = false;

  kpage = palloc_get_page (PAL_USER | PAL_ZERO);
  if (kpage != NULL) 
    {
	  // Added by Adrian Colesa
	  /*
		printf("[setup_stack] The stack virtual page %d starting at virtual address 0x%x will be mapped onto the kernel virtual page %d (physical frame %d) starting at kernel virtual address 0x%x (physical address 0x%x)\n", (((unsigned int) PHYS_BASE) - PGSIZE)/PGSIZE, (((uint8_t *) PHYS_BASE) - PGSIZE), (unsigned int)kpage/PGSIZE, ((unsigned int)vtop(kpage))/PGSIZE, kpage, vtop(kpage));
	   */

      success = install_page (((uint8_t *) PHYS_BASE) - PGSIZE, kpage, true);
      if (success) {
    	  // Changed by Adrian Colesa
        *esp = PHYS_BASE - 12;
      }
      else
        palloc_free_page (kpage);
    }
  return success;
}

/* Adds a mapping from user virtual address UPAGE to kernel
   virtual address KPAGE to the page table.
   If WRITABLE is true, the user process may modify the page;
   otherwise, it is read-only.
   UPAGE must not already be mapped.
   KPAGE should probably be a page obtained from the user pool
   with palloc_get_page().
   Returns true on success, false if UPAGE is already mapped or
   if memory allocation fails. */
static bool
install_page (void *upage, void *kpage, bool writable)
{
  struct thread *t = thread_current ();

  /* Verify that there's not already a page at that virtual
     address, then map our page there. */
  return (pagedir_get_page (t->pagedir, upage) == NULL
          && pagedir_set_page (t->pagedir, upage, kpage, writable));
}



/* Added by Adrian Colesa - multithreading */

static void
start_process_uthread(struct uthread_args *th_arg);

/* Starts a new user thread running a user function in the current process */
tid_t
process_uthread_execute (struct uthread_args *th_arg)
{
  tid_t tid;

  /* Create a new thread to execute the user specified function */
  tid = thread_create (th_arg->th_name, PRI_DEFAULT, start_process_uthread, th_arg);
  if (tid == TID_ERROR)
    return -1;

  return tid;
}

/* A user thread that starts running a user function */
static void
start_process_uthread(struct uthread_args *th_arg)
{
	  struct intr_frame if_;
	  bool success;

	  /* Initialize interrupt frame and load executable. */
	  memset (&if_, 0, sizeof if_);
	  if_.gs = if_.fs = if_.es = if_.ds = if_.ss = SEL_UDSEG;
	  if_.cs = SEL_UCSEG;
	  if_.eflags = FLAG_IF | FLAG_MBS;

	  // Here comes user thread's stack preparation
	  // Set the stack. Divide the stack page allocated at the end of address space,
	  // just before PHYS_BASE, between the TH_NO+1 threads:
	  // the main thread receives first half of the page and the other half to
	  // the other TH_NO threads, created after the main one
	  if_.esp = (PHYS_BASE - PGSIZE/2) - (th_arg->th_no * ((int)(PGSIZE/2)/TH_NO));

	  // Set the new uthread pagedir to be the same as that of its process and activate it
	  thread_current()->pagedir = th_arg->th_pagedir;
	  process_activate();

	  // Set the function argument on the stack
	  if_.esp -= 4;
	  *((int*)if_.esp) = th_arg->th_fc_arg;

	  if_.esp -= 4;
	  //*((int*)if_.esp) = return address;
	  // does not matter as long as the thread calls uthread_exit
	  // in the future this must be fixed such that the thread function returns in the thread_exit, even if not explicitly called

	  // Set the EIP to the address of function
	  if_.eip = (void (*) (void)) th_arg->th_fc_addr;

	  // Set the thread pid that of the main thread of the process it belongs to
	  thread_current()->pid = th_arg->th_pid;
	  thread_current()->uthread_id = th_arg->th_no;

	  /* Start the user thread by simulating a return from an
	     interrupt, implemented by intr_exit (in
	     threads/intr-stubs.S).  Because intr_exit takes all of its
	     arguments on the stack in the form of a `struct intr_frame',
	     we just point the stack pointer (%esp) to our stack frame
	     and jump to it. */
	  asm volatile ("movl %0, %%esp; jmp intr_exit" : : "g" (&if_) : "memory");
	  NOT_REACHED ();
}


/* Allocate and load a new page from the executable */
bool lazy_loading_page_for_address(uint8_t *upage)
{
	struct thread *crt = thread_current();
	struct supl_pte *spte;

	uint32_t pg_no = ((uint32_t) upage) / PGSIZE;

	IF_PRINT1 printf("Looking for page no %d (address 0x%x) in the supplemental hash table\n", pg_no, upage);

	spte = page_lookup(pg_no);

	if (spte == NULL) {
		IF_PRINT1 printf("Needed page was not found in the supplemental page table\n");
		return false;
	}

	IF_PRINT1 printf("Needed page was found in the supplemental page table\n");

    /* Get a page of memory. */
	uint8_t *kpage = palloc_get_page (PAL_USER);
	if (kpage == NULL)
	return false;

	// Establish the file offset the page must be read from
	file_seek (crt->exec_file, spte->ofs);

	// Added by Adrian Colesa - VM
	IF_PRINT1 printf("\n[load_segment] Virtual page %d: from offset %d in the executable file read %d bytes and zero the rest %d bytes\n", ((unsigned int) upage)/PGSIZE, file_tell(crt->exec_file), spte->page_read_bytes, spte->page_zero_bytes);

	/* Load this page. */
	if (file_read (crt->exec_file, kpage, spte->page_read_bytes) != (int) spte->page_read_bytes)
	{
		palloc_free_page (kpage);
		return false;
	}
	memset (kpage + spte->page_read_bytes, 0, spte->page_zero_bytes);

	// Added by Adrian Colesa - Userprog + VM
	//printf("[load_segment] The process virtual page %d starting at virtual address 0x%x will be mapped onto the kernel virtual page %d (physical frame %d) starting at kernel virtual address 0x%x (physical address 0x%x)\n", ((unsigned int) upage)/PGSIZE, upage, (unsigned int)kpage/PGSIZE, ((unsigned int)vtop(kpage))/PGSIZE, kpage, vtop(kpage));
	IF_PRINT1 printf("[load_segment] Virtual page %d (vaddr=0x%x): mapped onto the kernel virtual page %d (physical frame %d)\n", ((unsigned int) upage)/PGSIZE, upage, (unsigned int)kpage/PGSIZE, ((unsigned int)vtop(kpage))/PGSIZE);

	/* Add the page to the process's address space. */
	if (!install_page (spte->virt_page_addr, kpage, spte->writable))
	{
		palloc_free_page (kpage);
		return false;
	}

	return true;
}
