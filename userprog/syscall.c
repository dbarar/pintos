#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"

/* Added by Adrian Colesa - multithreading */
#include "userprog/process.h"
#include "threads/synch.h"

//Added by Radu
#include "threads/vaddr.h"
#include "filesys/directory.h"
// end Radu

/* Added by Adrian Colesa - multithreading */
static struct semaphore join_sema[TH_NO];
static struct uthread_args process_uthreads_args[TH_NO];
static tid_t process_uthreads_tids[TH_NO];
static int process_uthread_status[TH_NO];

static void syscall_handler (struct intr_frame *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

/*
 * Added by Adrian Colesa - multithreading
 * This should be normally associated to each process, but since we currently support
 * just one process, declared it global
 */
static crt_thread_id = 0;

//Radu's code


open_file* search_file(int fd) {
	struct thread* t = thread_current();

		struct list_elem *e;


		for (e = list_begin (&t->open_files); e != list_end (&t->open_files);
			e = list_next (e))
		{
			open_file* opened_file = (open_file*)malloc(sizeof(open_file));
			opened_file = list_entry (e, open_file, elem);
			if(opened_file->fd == fd) {
				return opened_file;
			}
		}
		return NULL;
}

bool has_file_opened(int fd) {

	open_file* opened_file = (open_file*)malloc(sizeof(open_file));

	opened_file = search_file(fd);
	if(opened_file->file == NULL) {
		return false;
	}
	return true;
}
//end Radu


/* Reads a byte at user virtual address UADDR.
UADDR must be below PHYS_BASE.
Returns the byte value if successful, -1 if a segfault
occurred. */
static int
get_user (const uint8_t *uaddr)
{
	if(uaddr < 0x08048000 || !is_user_vaddr((void*) uaddr)) {
    	return -1;
	}
	
	int result;
	asm ("movl $1f, %0; movzbl %1, %0; 1:": "=&a" (result) : "m" (*uaddr));
	return result;
}
/* Writes BYTE to user address UDST.
UDST must be below PHYS_BASE.
Returns true if successful, false if a segfault occurred. */
static bool
put_user (uint8_t *udst, uint8_t byte)
{
	if (!is_user_vaddr((void*) udst)) {
    	return false;
	}

	int error_code;
	asm ("movl $1f, %0; movb %b2, %1; 1:": "=&a" (error_code), "=m" (*udst) : "q" (byte));
	return error_code != -1;
}

bool verifyAddr(void *addr) {
	if(addr < 0x08048000 || !is_user_vaddr(addr) ) {
		return false;
	}
	if(pagedir_get_page(thread_current()->pagedir, addr) == NULL) {
		return false;
	}
	return true;
}

static int
read_arguments_and_validate(void *src, void *dst, size_t bytes){
	for(size_t i = 0; i < bytes; i++){
		int32_t value = get_user(src + i);
		if(value == -1)
			thread_exit(-1);
		*(char *) (dst + i) = value & 0xff;
	}
	return (int) bytes;
}
// Denisa end


static void
syscall_handler (struct intr_frame *f)
{
	/* Added by Adrian Colesa - multithreading */

	int syscall_no;
	int fd, no;
	char *buf;
	int waited_tid;
	int *status, th_status, i;
	int exit_code;

	//Denisa:
	read_arguments_and_validate(f->esp, &syscall_no, sizeof(syscall_no));
	// end Denisa;

	IF_PRINT1 printf ("system call no %d!\n", syscall_no);	

	int* stack = (int*)f->esp;
	char *fileName;
	const void *buffer;
	unsigned size;
	bool toBeReturnedBool = false;
	bool testSuccess = false;
	unsigned new_pos;

	struct file* current_file = NULL;
	open_file* opened_file_var = (open_file*)malloc(sizeof(open_file));


	int bytes_no;
	int charsCount = 0;

	/* Added by Adrian Colesa - multithreading */
	switch (syscall_no) {
		case SYS_HALT:
			shutdown_power_off();
			return;
		case SYS_EXIT:
			IF_PRINT1 printf ("SYS_EXIT system call from thread %d!\n", thread_current()->tid);
			read_arguments_and_validate(f->esp + 4, &exit_code, sizeof(exit_code));
			thread_exit(exit_code);
			return;
		case SYS_EXEC:
			IF_PRINT1 printf ("SYS_EXEC system call from thread %d!\n", thread_current()->tid);
			void * cmd;
			read_arguments_and_validate(f->esp + 4, &cmd, sizeof(cmd));
			if(!verifyAddr(cmd))
				thread_exit(-1);
			IF_PRINT1 printf("A trecut de validare\n");
			buf = (char *) cmd;
			IF_PRINT1 printf("BUF = %s\n", buf);
			th_status = process_execute(buf);
			f->eax = th_status;
			return;
		case SYS_WAIT:
			IF_PRINT1 printf ("SYS_WAIT system call from thread %d!\n", thread_current()->tid);
			read_arguments_and_validate(f->esp + 4, &waited_tid, sizeof(waited_tid));
			exit_code = process_wait(waited_tid);
			f->eax = exit_code;
			return;
		case SYS_CREATE:
			IF_PRINT1 printf ("SYS_CREATE system call from thread %d!\n", thread_current()->tid);
			read_arguments_and_validate(f->esp + 4, &fileName, sizeof(fileName));
			read_arguments_and_validate(f->esp + 8, &size, sizeof(size));

			if(!verifyAddr(fileName))
				thread_exit(-1);

			IF_PRINT1 printf("A trecut de validare\n");

			IF_PRINT1 printf("File name: %s\n", fileName);
			IF_PRINT1 printf("Size: %d\n",(int*) size);

			while(fileName[charsCount] != '\0') {
				charsCount++;
			}
			IF_PRINT1 printf("charsCount: %d\n");
			IF_PRINT1 printf("filename: %s\n", fileName);
			
			if(charsCount < 1 || fileName == "" || fileName == NULL) {
				IF_PRINT1 printf("BAD FILENAME %d\n", f->eax);
				thread_exit(-1);
				return;
			}

			toBeReturnedBool = filesys_create(fileName, size);
			f->eax = toBeReturnedBool;
			testSuccess = toBeReturnedBool;
			IF_PRINT1 printf("OK-CREATE  %d\n", f->eax);
			return;
		case SYS_WRITE:
			IF_PRINT1 printf ("SYS_WRITE system call from thread %d!\n", thread_current()->tid);

			read_arguments_and_validate(f->esp + 4, &fd, sizeof(fd));
			read_arguments_and_validate(f->esp + 8, &buffer, sizeof(buffer));
			read_arguments_and_validate(f->esp + 12, &size, sizeof(size));

			if(!verifyAddr(buffer))
				thread_exit(-1);
			if(!verifyAddr(buffer + size - 1))
				thread_exit(-1);

			IF_PRINT1 printf("A trecut de validare\n");

			IF_PRINT1 printf("Buffer: %s\n", buffer);
			IF_PRINT1 printf("Size: %d\n",(int*) size);
			IF_PRINT1 printf("File descriptor: %d\n",fd);
			
			if(fd == 0) {
				IF_PRINT1 printf("BAD FILE DESCRIPTOR\n");
				thread_exit(-1);
				return;
			}
			
			if(size < 0) {
				IF_PRINT1 printf("E: size invalid\n");
				thread_exit(-1);
			}


			if(fd > thread_current()->fd) {
					thread_exit(-1);
			}

			if(fd == 1) {
				putbuf(buffer, size);
				f->eax = size;
				return;
			}
			else {
				if(has_file_opened(fd)) {
					opened_file_var = search_file(fd);
					if(opened_file_var->file == NULL) {
						IF_PRINT1 printf("E null\n");
					}
					bytes_no = file_write(opened_file_var->file, buffer, size);
					f->eax = bytes_no;
					return;
				} else {
					IF_PRINT1 printf("Dont have file opened\n");
					thread_exit(-1);
					return;
				}
			}
			f->eax = 0;

			return;

		case SYS_READ:
			IF_PRINT1 printf ("SYS_READ system call from thread %d!\n", thread_current()->tid);

			read_arguments_and_validate(f->esp + 4, &fd, sizeof(fd));
			read_arguments_and_validate(f->esp + 8, &buffer, sizeof(buffer));
			read_arguments_and_validate(f->esp + 12, &size, sizeof(size));

			if(!verifyAddr(buffer))
				thread_exit(-1);
			if(!verifyAddr(buffer + size - 1))
				thread_exit(-1);

			IF_PRINT1 printf("A trecut de validare\n");
			
			if(fd == 1) {
				IF_PRINT1 printf("BAD FILE DESCRIPTOR\n");
				thread_exit(-1);
				return;

			}
			if(size < 0) {
				IF_PRINT1 printf("E: size invalid\n");
				thread_exit(-1);
			}

			//read from console
			if(fd == 0) {
				unsigned i;
				for(i = 0; i < size; i++) {
					if(! put_user(buffer + i, input_getc()) ) {
						thread_exit(-1);
					}
				}
				f->eax = i;
				return;
			}
			if(fd > thread_current()->fd) {
					thread_exit(-1);
			}
			else {
				if(has_file_opened(fd)) {
					opened_file_var = search_file(fd);
					bytes_no = file_read(opened_file_var->file,buffer,size);
					f->eax = bytes_no;
					return;
				} else {
					IF_PRINT1 printf("Dont have file opened\n");
					thread_exit(-1);
					return;
				}
			}
			f->eax = 0;

			return;

		case SYS_FILESIZE:
			IF_PRINT1 printf ("SYS_READ system call from thread %d!\n", thread_current()->tid);

			read_arguments_and_validate(f->esp + 4, &fd, sizeof(fd));

			IF_PRINT1 printf("A trecut de validare\n");

			if(fd == 1 || fd == 0) {
				IF_PRINT1 printf("BAD FILE DESCRIPTOR\n");
				thread_exit(-1);
				return;

			}


			if(has_file_opened(fd)) {
				opened_file_var = search_file(fd);
				bytes_no = file_length(opened_file_var->file);
				f->eax = bytes_no;
				return;
			} else {
				printf("Dont have file opened\n");
				IF_PRINT1 thread_exit(-1);
				return;
			}


			f->eax = 0;

			return;

		case SYS_OPEN:
			IF_PRINT1 printf ("SYS_OPEN system call from thread %d!\n", thread_current()->tid);

			read_arguments_and_validate(f->esp + 4, &fileName, sizeof(fileName));
			if(!verifyAddr(fileName))
				thread_exit(-1);

			IF_PRINT1 printf("A trecut de validare\n");

			while(fileName[charsCount] != '\0') {
				charsCount++;
			}

			if(charsCount < 1 || fileName == "" || charsCount > NAME_MAX || fileName == NULL) {
				IF_PRINT1 printf("BAD FILENAME\n");
				f->eax = -1;
				return;
			}

			struct file* file=  filesys_open(fileName);

			if(file == NULL) {
				IF_PRINT1 printf("File doesn't exist\n");
				f->eax = -1;
				f->error_code = -1;
				IF_PRINT1 printf("NULLLL\n");
				// thread_exit(-1);
				IF_PRINT1 printf("PAGEFAULT\n");
				return;
			}

			struct thread* t = thread_current();

			opened_file_var->fd = t->fd++;
			opened_file_var->filename = fileName;
			opened_file_var->file = file;

			list_push_back(&t->open_files,&opened_file_var->elem);
			f->eax = opened_file_var->fd;
			IF_PRINT1 printf("Opened %s with fd = %d\n", opened_file_var->filename, f->eax);

			return;

		case SYS_CLOSE:
			IF_PRINT1 printf ("SYS_CLOSE system call from thread %d!\n", thread_current()->tid);
			
			read_arguments_and_validate(f->esp + 4, &fd, sizeof(fd));

			int max_fd = thread_current()->fd;
			if(fd > max_fd) {
				IF_PRINT1 printf("Bad file descriptor\n");
				thread_exit(-1);
				return;
			}
			open_file* file_to_be_closed = search_file(fd);
			if(file_to_be_closed == NULL) {
				IF_PRINT1 printf("Bad file descriptor\n");
				thread_exit(-1);
				return;
			}

			list_remove(&file_to_be_closed->elem);
			file_close(file_to_be_closed->file);
			return;		

		case SYS_SEEK:
			IF_PRINT1 printf ("SYS_SEEK system call from thread %d!\n", thread_current()->tid);
			
			read_arguments_and_validate(f->esp + 4, &fd, sizeof(fd));
			read_arguments_and_validate(f->esp + 8, &new_pos, sizeof(new_pos));

			if(fd == 0 || fd == 1) {
				printf("BAD FILE DESCRIPTOR\n");
				thread_exit(-1);
				return;
			}
			if(has_file_opened(fd)) {
				opened_file_var = search_file(fd);
				file_seek(opened_file_var->file,new_pos);
			} else {
				printf("Dont have file opened\n");
				thread_exit(-1);
				return;
			}
			f->eax = new_pos;
			return;

		case SYS_UTHREAD_CREATE:
			IF_PRINT1 printf ("SYS_UTHREAD_CREATE system call from thread %d!\n", thread_current()->tid);

			/* Thread function and its argument prepared for thread creation */
			process_uthreads_args[crt_thread_id].th_fc_addr = ((int*)f->esp)[1];
			process_uthreads_args[crt_thread_id].th_fc_arg = ((int*)f->esp)[2];

			/* Thread name is based on the process' one and thread no */
			snprintf(process_uthreads_args[crt_thread_id].th_name,
					sizeof(process_uthreads_args[crt_thread_id].th_name),
					"th-%d", crt_thread_id);

			/* Thread pid equals that of the current process */
			process_uthreads_args[crt_thread_id].th_pid = thread_current()->pid;

			/* Thread pagedir equals that of the current process */
			process_uthreads_args[crt_thread_id].th_pagedir = thread_current()->pagedir;

			/* Process internal thread id (no) */
			process_uthreads_args[crt_thread_id].th_no = crt_thread_id;

			/* Call the function to create the thread with the prepared arguments */
			process_uthreads_tids[crt_thread_id] = process_uthread_execute(&process_uthreads_args[crt_thread_id]);

			/* Check if thread successfully created */
			if (process_uthreads_tids[crt_thread_id] != -1) {
				/* TO DO: Initialize the corresponding semaphore for possible joiner */
				// ...
				sema_init(&join_sema[crt_thread_id], 0);

				f->eax = crt_thread_id;
			} else {
				f->eax = -1;
			}

			crt_thread_id++;
			return;
		case SYS_UTHREAD_JOIN:
			IF_PRINT1 printf ("SYS_UTHREAD_JOIN system call from thread %d!\n", thread_current()->tid);

			waited_tid = ((int*)f->esp)[1];
			status = (int*)((int*)f->esp)[2];

			if (process_uthreads_tids[waited_tid] != -1) {
				/* TO DO: Use the corresponding semaphore to wait for the desired thread */
				// ....
				sema_down(&join_sema[waited_tid]);

				/* TO DO: Return the exit code of the waited thread */
				// ...
				

				/* Return success */
				f->eax = 0;
			} else
				/* Return error - non existent thread */
				f->eax = -1;

			return;
		case SYS_UTHREAD_EXIT:
			IF_PRINT1 printf ("SYS_UTHREAD_EXIT system call from thread %d!\n", thread_current()->tid);
			th_status = ((int*)f->esp)[1];

			/* TO DO: Save the exit code for a possible joining thread */
			// ...

			/* TO DO: use the corresponding semaphore to wake up the possible blocked joining thread */
			// ...

			// Terminate the thread
			thread_exit(0);
			// not reached

			f->eax=0;
			return;
		case SYS_UTHREAD_GETPID:
			IF_PRINT1 printf ("SYS_UTHREAD_GETPID system call from thread %d!\n", thread_current()->tid);

			/* TO DO: return the process pid */
			// ...

			return;
		case SYS_UTHREAD_GETTID:
			IF_PRINT1 printf ("SYS_UTHREAD_GETTID system call from thread %d!\n", thread_current()->tid);

			/* TO DO: return the thread id: this could be tid or the internal thread number */
			// ...

			return;
	}

	thread_exit (0);
}
